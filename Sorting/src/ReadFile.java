import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;

public class ReadFile {
	
	public static File file;
	
	public ArrayList<Eintrag> entries;
	
	public ReadFile(String f)
	{
		this.file = new File(f);
		entries = new ArrayList<Eintrag>();
	}
	
	public void readInformation() throws IOException
	{
		FileInputStream  fr = new FileInputStream (file);
		BufferedReader bf = new BufferedReader(new InputStreamReader(fr));
		
		bf.readLine();
		for(String t = bf.readLine(); t != null;t=bf.readLine())
		{
			
			Eintrag entry = new Eintrag();
			t = t.replaceAll("ü", "ue")
	             .replaceAll("ö", "oe")
	             .replaceAll("ä", "ae")
	             .replaceAll("ß", "ss");
			
			if((t.length() - t.replace("$", "").length()) < 4)
				continue;
		
			
			String[] parts = t.split("\\$");
				
			entry.setType(parts[0]);
			DateTimeFormatter f = DateTimeFormatter.ofPattern("dd.MM.yyyy HH:mm:ss");
			LocalDateTime time = LocalDateTime.parse(parts[1], f);
			entry.timedate = time;
			entry.quelle = parts[2];
			entry.id = Integer.parseInt(parts[3]);
			
			StringBuilder b = new StringBuilder();
			for(int i = 4; i < parts.length; i++)
				b.append(parts[i]);
			
			entry.beschreibung = b.toString();
			
			entries.add(entry);
			
		}
		bf.close();
		fr.close();
	}
	
	
	public void deleteUnimportant()
	{
		Iterator<Eintrag> t = entries.iterator();
		
		while(t.hasNext())
		{
			Eintrag e = t.next();
			if(e.KOPIEtypeKOPIE.equals("informationen"))
				t.remove();
		}
	}
	
	public String print()
	{
		StringBuilder b = new StringBuilder();
		
		b.append("Entries: "+entries.size());
		b.append(System.lineSeparator());
		entries.sort((x,y) -> x.id - y.id);
		
		
			for(Eintrag e : entries) {
				b.append(e.toString());
				b.append(System.lineSeparator());
			}
		
		return b.toString();
	}
	
	
	public String groupID()
	{
		HashMap<Integer, ArrayList<Eintrag>> map = new HashMap<>();
		
		for(Eintrag e : entries)
		{
			if(!map.containsKey(e.id))
				map.put(e.id, new ArrayList<Eintrag>());
		}
		
		for(Integer i : map.keySet())
		{
				for(Object e : entries.stream().filter(x -> x.id == i).toArray())
				{
					map.get(i).add((Eintrag) e);
				}
		}
		
		for(Entry<Integer, ArrayList<Eintrag>> eintrag : map.entrySet())
		{
			System.out.println(eintrag.getKey() + ": "+eintrag.getValue().size() + " Eintraege");
			StringBuilder b = new StringBuilder();
			
			for(Eintrag g : eintrag.getValue())
				b.append(g.toString()+System.lineSeparator());
			
			
			System.out.println(b.toString());
		}
		
		return "";
	}
	
	public int countUnerwartetHeruntergerfahren()
	{
		int count = 0;
		
		StringBuilder b = new StringBuilder();
		
		for(Eintrag a : entries)
		{
			if(a.beschreibung.contains("unerwartet heruntergefahren")) {
				count++; b.append(a.toString() + System.lineSeparator());}
		}
		System.out.println("UNERWARTET HERUNTERGEFAHREN: "+count);
		System.out.println(b.toString());
		return count;
	}
	
	public int countUnerwartetBeendet()
	{
		int count = 0;
		StringBuilder b = new StringBuilder();
		
		for(Eintrag a : entries)
		{
			if(a.beschreibung.contains("wurde unerwartet beendet")) {
				count++; b.append(a.toString() + System.lineSeparator());}
		}
		System.out.println("UNERWARTET BEENDETE DIENSTE: "+count);
		System.out.println(b.toString());
	
		return count;
		
	}
	
	public int countDNSFail()
	{
		int count = 0;
		StringBuilder b = new StringBuilder();
		
		for(Eintrag a : entries)
		{
			if(a.id == 1014) {
				count++; b.append(a.toString() + System.lineSeparator());}
		}
		System.out.println("ZEIT�BERSCHREITUNG BEI DER NAMENSAUFL�SUNG: "+count);
		System.out.println(b.toString());
	
		return count;
		
	}
	
	public enum EintragTyp
	{
		INFORMATION, FEHLER, KRITISCH, WARNING, UNBEKANNT; 
	}

}
