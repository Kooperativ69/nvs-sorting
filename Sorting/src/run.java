import java.io.IOException;


public class run {

	public static void main(String[] args) {
		ReadFile f = new ReadFile("J:\\Project Files\\sys.txt");
		long i = System.currentTimeMillis();
		try {
			f.readInformation();
			f.deleteUnimportant();
		} catch (IOException e) {
			
			e.printStackTrace();
		}
		//System.out.println(f.print());
		f.groupID();
		
		
		
		
		System.out.println("KRITISCHE EINTRÄGE: " + f.entries.stream().filter(x -> x.KOPIEtypeKOPIE.equals("kritisch")).count());
		System.out.println("WARNUNGEN: " + f.entries.stream().filter(x -> x.KOPIEtypeKOPIE.equals("warnung")).count());
		System.out.println("INFORMATIONEN: " + f.entries.stream().filter(x -> x.KOPIEtypeKOPIE.equals("informationen")).count());
		System.out.println("FEHLER: " + f.entries.stream().filter(x -> x.KOPIEtypeKOPIE.equals("fehler")).count());
		System.out.println("UNBEKANNT: " + f.entries.stream().filter(x -> !x.KOPIEtypeKOPIE.equals("kritisch"))
															 .filter(x -> !x.KOPIEtypeKOPIE.equals("warnung"))
															 .filter(x -> !x.KOPIEtypeKOPIE.equals("informationen"))
															 .filter(x -> !x.KOPIEtypeKOPIE.equals("fehler"))
															 .count());
		
		System.out.println();
		
		f.countUnerwartetHeruntergerfahren();
		f.countUnerwartetBeendet();
		f.countDNSFail();
		
		
		long b = System.currentTimeMillis();
		long c = b-i;	
		System.out.println(c + " Millisekunden hat der Vorgang gedauert");
	}
}
